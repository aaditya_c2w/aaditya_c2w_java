class Size{
	public static void main(String[] args){
		String size="XL";
		switch(size){
			case "XL":
				System.out.println("Extra large");
				break;
			case "S":
				System.out.println("Small");
				break;
			case "M":
				System.out.println("Medium");
				break;
			case "L":
				System.out.println("Large");
				break;
			case "XXL":
				System.out.println("Double XL");
				break;
			default:
				System.out.println("Wrong Input");
		}
	}
}
