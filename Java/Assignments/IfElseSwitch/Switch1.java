class Odd{
	public static void main(String[] args){
		int num=45;
		switch(num%2){
			case 0:
				System.out.println(num+" is an Odd number");
				break;
			case 1:
				System.out.println(num+" is an Even number");
				break;
			default:
				System.out.println("Zero");
		}
	}
}
