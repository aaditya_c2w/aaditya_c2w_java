class Grade{
	public static void main(String[] args){
		char ch='A';
		switch(ch){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'A':
				System.out.println("Excellent");
				break;
			case 'B':
				System.out.println("Very Good");
				break;
			case 'C':
				System.out.println("Good");
				break;
			case 'D':
				System.out.println("Poor");
				break;
			case 'E':
				System.out.println("Very Poor");
				break;
			case 'F':
				System.out.println("Fail");
				break;
			default:
				System.out.println("Invalid Input");
		}
	}
}
