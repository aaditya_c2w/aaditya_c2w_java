class Trip{
	public static void main(String[] args){
		int i=15000;
		switch(i){
			case 15000:
				System.out.println("For budget 15000 destination is Jammu and Kashmir");
				break;
			case 10000:
				System.out.println("For budget 10000 destination is Manali");
				break;
			case 6000:
				System.out.println("For budget 6000 destination is Amritsar");
				break;
			case 2000:
				System.out.println("For budget 2000 destination is Mahabaleshwar");
				break;
			default:
				System.out.println("For Other budgets try next time");
		}
	}
}
