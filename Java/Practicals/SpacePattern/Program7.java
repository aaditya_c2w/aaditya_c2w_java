import java.util.*;
class Pattern{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=1;
			for(int sp=1;sp<i;sp++){
				System.out.print(" "+" ");
			}
			for(int j=row;j>=i;j--){
				System.out.print(num++ +" ");
			}
			System.out.println();
		}
	}
}
