import java.util.*;
class C2W{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch=65+row-i;
			for(int sp=1;sp<=row-i;sp++){
				System.out.print(" "+" ");
			}
			for(int j=1;j<=i;j++){
				System.out.print((char)ch++ +" ");
			}
			System.out.println();
		}
	}
}
