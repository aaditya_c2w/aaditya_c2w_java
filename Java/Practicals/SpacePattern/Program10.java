import java.util.*;
class Last{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int ch=65+i-1;
			for(int sp=1;sp<i;sp++){
				System.out.print(" "+" ");
			}
			for(int j=row;j>=i;j--){
				if(j%2==0){
					System.out.print(ch +" ");
				}
				else{
					System.out.print((char)ch + " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}
