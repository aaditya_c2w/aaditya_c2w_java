import java.util.*;
class C2W{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Rows : ");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			int num=row;
			for(int sp=row;sp>i;sp--){
				System.out.print(" " + " ");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
					System.out.print(num-- +" ");
				}
				else{
					System.out.print(num++ +" ");
				}
			}
			System.out.println();
		}
	}
}
