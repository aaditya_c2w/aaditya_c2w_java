import java.util.*;
class Demo{
	public static void main(String[] args){
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter Array Size : ");
		int size=sc.nextInt();
		int EmpId[]=new int[size];
		for(int i=0;i<size;i++){
			System.out.println("Enter Element : ");
			EmpId[i]=sc.nextInt();
		}
		int sum=0;
		for(int i=0;i<size;i++){
			if(EmpId[i]%2==1){
				sum=sum+EmpId[i];
			}
		}
		System.out.println("Sum of Odd Elements is : " +sum);
	}
}
