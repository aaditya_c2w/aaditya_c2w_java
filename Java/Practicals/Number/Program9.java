import java.io.*;
class Pal{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");
		int num=Integer.parseInt(br.readLine());
		int temp=num;
		int var=0;
		int rem=0;
		while(num>0){
			rem=num%10;
			var=var*10+rem;
			num/=10;
		}
		if(var==temp){
			System.out.println(temp+ " is a palindrome number");
		}
		else{
			System.out.println(temp+" is not a palindrome number");
		}
	}
}
