import java.io.*;
class Number{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");
		int num=Integer.parseInt(br.readLine());
		int fact=num;
		int count=0;
		while(fact>=1){
			if(num%fact==0){
				count++;
			}
			fact--;
		}
		if(count==2){
			System.out.println(num +" is a prime number");
		}else{
			System.out.println(num + " is not a prime number");
		}
	}
}
