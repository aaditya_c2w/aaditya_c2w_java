import java.io.*;
class Composite{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");
		int num=Integer.parseInt(br.readLine());
		int fact=num;
		int count=0;
		while(fact>=1){
			if(num%fact==0){
				count++;
			}
			fact--;
		}
		if(count==2){
			System.out.println(num +" is not a composite number");
		}
		else{
			System.out.println(num +" is a composite number");
		}
	}
}
