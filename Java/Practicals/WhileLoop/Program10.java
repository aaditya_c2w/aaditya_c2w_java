class Sum{
	public static void main(String[] args){
		long num=9307922405l;
		int sum=0;
		long rem=1l;
		while(num>0){
			rem=num%10;
			num/=10;
			sum+=rem;
		}
		System.out.println("Sum of digits in 9307922405 is " + sum);
	}
}
