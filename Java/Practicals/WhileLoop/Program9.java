class Count{
	public static void main(String[] args){
		int num=214367689;
		int odd=0;
		int even=0;
		int rem=1;
		while(num>0){
			rem=num%10;
			num/=10;
			if(rem%2==1){
				odd++;
			}
			else{
				even++;
			}
		}
		System.out.println("Odd Count : " +odd);
		System.out.println("Even Count : "+even);
	}
}
