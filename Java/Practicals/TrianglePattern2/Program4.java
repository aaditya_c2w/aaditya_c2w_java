import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter row : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int ch1=64+row;
			int ch2=96+row;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char) ch2-- +" ");
				}
				else{
					System.out.print((char) ch1-- +" ");
				}
			}
			System.out.println();
		}
	}
}
