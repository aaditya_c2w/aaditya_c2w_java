class Upper{
	public static void main(String[] args){
		char ch='a';
		if(ch<='z' && ch>='a'){
			System.out.println(ch+" is a lowercase character");
		}
		else if(ch>='A' && ch<='Z'){
			System.out.println(ch+" is an Uppercase character");
		}
		else {
			System.out.println("Wrong Input");
		}
	}
}
