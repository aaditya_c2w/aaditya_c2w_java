class Pass{
	public static void main(String[] args){
		float num=74f;
		if(num<=100 && num>=75){
			System.out.println("Passed : First class with distinction");
		}
		else if(num<75 && num>=60){
			System.out.println("Passed : First class");
		}
		else if(num<60 && num>=50){
			System.out.println("Passed : Second class");
		}
		else if(num<50 && num>=35){
			System.out.println("Pass");
		}
		else if(num>100 && num<0){
			System.out.println("Invalid Input");
		}
		else{
			System.out.println("Fail");
		}
	}
}
