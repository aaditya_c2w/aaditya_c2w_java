import java.io.*;
class Buff{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=row;i>=1;i--){
			int num=1;
			char ch='a';
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(num++ + " ");
				}
				else{
					System.out.print(ch++ + " ");
				}
			}
			System.out.println();
		}
	}
}
