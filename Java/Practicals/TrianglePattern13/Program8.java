import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=row+1-i;
			int ch=64+num;
			for(int j=row;j>=i;j--){
				if(i%2==1){
					System.out.print(num-- +" ");
				}
				else{
					System.out.print((char)ch-- + " ");
				}
			}
			System.out.println();
		}
	}
}
