import java.io.*;
class C2W{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Rows : ");
		int row=Integer.parseInt(br.readLine());
		int num=2;
		for(int i=1;i<=row;i++){
			for(int j=row;j>=i;j--){
				System.out.print(num + " ");
				num+=2;
			}
			System.out.println();
		}
	}
}
