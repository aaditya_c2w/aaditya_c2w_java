import java.io.*;
class Square{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number : ");
		long num=Long.parseLong(br.readLine());               //45632985632
		long rev=0L;
		long rem=0L;
		while(num>0){
			rem=num%10;
			rev=rev*10+rem;
			num/=10;
		}
		while(rev>0){
			rem=rev%10;
			if(rem%2==1){
				System.out.print(rem*rem + ",");
			}
			rev/=10;
		}
		System.out.println();
	}
}
