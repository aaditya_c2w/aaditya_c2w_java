import java.io.*;
class Table{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=row;i>=1;i--){
			int temp=1;
			for(int j=row;j>=i;j--){
				System.out.print(i*temp +" ");
				temp++;
			}
			System.out.println();
		}
	}
}
