class Div{
	public static void main(String[] args){
		int num=2569185;
		int rem=1;
		System.out.print("Digits divisible by 2 are : ");
		while(num>0){
			rem=num%10;
			if(rem%2==0){
				System.out.print(rem +" ");
			}
			num=num/10;
		}
	}
}
