class Number{
	public static void main(String[] args){
		int num=2469185;
		int rem=1;
		int sum=0;
		int sq=1;
		while(num>0){
			rem=num%10;
			if(rem%2==1){
				sq=rem*rem;
				sum=sum+sq;
			}
			num/=10;
		}
		System.out.print(sum);
	}
}
