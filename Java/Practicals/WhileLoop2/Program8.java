class Product{
	public static void main(String[] args){
		int num=256985;
		int rem=1;
		int product=1;
		while(num>0){
			rem=num%10;
			if(rem%2==1){
				product=product*rem;
			}
			num/=10;
		}
		System.out.print("Product of Odd Digits: " + product);
	}
}
