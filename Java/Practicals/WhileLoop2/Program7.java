class Sum{
	public static void main(String[] args){
		int num=256985;
		int rem=1;
		int sum=0;
		System.out.print("Sum of even digits : ");
		while(num>0){
			rem=num%10;
			if(rem%2==0){
				sum=sum+rem;
			}
			num/=10;
		}
		System.out.print(sum);
	}
}
