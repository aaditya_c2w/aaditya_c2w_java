class SumProduct{
	public static void main(String[] args){
		int num=9367924;
		int rem=1;
		int sum=0;
		int product=1;
		while(num>0){
			rem=num%10;
			if(rem%2==0){
				product=product*rem;
			}
			else{
				sum=sum+rem;
			}
			num/=10;
		}
		System.out.println("Sum of Even digits: " + sum);
		System.out.println("Product of Odd digits: " + product);
	}
}
