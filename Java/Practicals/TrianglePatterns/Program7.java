import java.io.*;
class Pattern{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Number of Rows : ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			int num=1;
			for(int j=row;j>=i;j--){
				System.out.print(num++ + " ");
			}
			System.out.println();
		}
	}
}
