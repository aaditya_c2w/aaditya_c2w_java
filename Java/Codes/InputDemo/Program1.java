import java.util.*;
class Odd{
	public static void main(String[] args){
		Scanner num = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num1=num.nextInt();
		if(num1%2==0){
			System.out.println(num1 + " is an even number");
		}
		else{
			System.out.println(num1 + " is an odd number");
		}
	}
}
