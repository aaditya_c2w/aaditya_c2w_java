import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Name : ");
		String name=br.readLine();

		System.out.print("Society name : ");
		String SocName=br.readLine();

		System.out.print("Wing : ");
		char wing=(char)br.read();
		br.skip(1);

		System.out.print("Flat no : ");
		int flatno=Integer.parseInt(br.readLine());

		System.out.println(" Name : " +name);
		System.out.println("Society Name : " +SocName);
		System.out.println(" Wing : " +wing);
		System.out.println(" Flat no : " +flatno);
	}
}
